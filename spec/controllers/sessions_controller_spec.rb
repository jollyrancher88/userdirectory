require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #create" do
    before :all do
       User.find_by(username: 'billybob') ||
          User.create(username: 'billybob',
                      first_name: 'Billy',
                      last_name: 'Bob',
                      password: 'super123',
                      password_confirmation: 'super123',
                      roles: [Roles::GUEST])
    end

    context 'login successful' do
      let(:user) { User.find_by(username: 'billybob') }
      it 'Redirects to /users' do
        post :create, params: {session: {username: user.username, password: 'super123'}}
        expect(response).to have_http_status(302)
        expect(response.body).to eq "<html><body>You are being <a href=\"http://test.host/users\">redirected</a>.</body></html>"
      end
    end

    context 'login failed' do
      let(:user) { User.find_by(username: 'billybob') }
      it 'Redirects to /users' do
        post :create, params: {session: {username: user.username, password: '123'}}
        expect(response).to have_http_status(200)
        expect(flash[:error]).to be_present
        expect(flash[:error]).to eq 'Login Failed!'
      end
    end
  end
end
