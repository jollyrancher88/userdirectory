require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  include SessionsHelper

  describe 'GET #index' do
    context 'A user is logged in' do
      before do
        guest_user = User.find_by(username: 'billybob') ||
                     User.create(username: 'billybob',
                                 first_name: 'Billy',
                                 last_name: 'Bob',
                                 password: 'super123',
                                 password_confirmation: 'super123',
                                 roles: [Roles::GUEST])
        log_in(guest_user)
      end

      after do
        log_out
      end

      it 'returns http success' do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    context 'No user logged in' do
      it 'returns 302 and redirects' do
        get :index
        expect(response).to have_http_status(302)
        expect(response.body).to include 'You are being '
        expect(response.body).to include 'redirected'
      end
    end
  end
end
