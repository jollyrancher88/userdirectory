require 'rails_helper'

RSpec.describe "users/index.html.erb", type: :view do
  include SessionsHelper

  let(:users) {User.all}

  context 'User Can Edit' do
    before :each do
      assign(:users, users)
      assign(:can_edit, true)
    end

    it 'displays the user list table' do
      render
      expect(rendered).to include('<table class="table table-striped table-bordered">')
      expect(rendered).to include('<td>Action(s)</td>')
      expect(rendered).to include('Change Password</button>')
      users.each do |user|
        expect(rendered).to include("<td>#{user.first_name}</td>")
        expect(rendered).to include("<td>#{user.last_name}</td>")
        expect(rendered).to include("<td>#{user.username}</td>")
        expect(rendered).to include("<td>#{user.roles.map{|u| u.name.capitalize}.join(',')}</td>")
      end
    end
  end

  context 'User Cannot Edit' do
    before :each do
      assign(:users, users)
      assign(:can_edit, false)
    end

    it 'displays the user list table' do
      render
      expect(rendered).to include('<table class="table table-striped table-bordered">')
      expect(rendered).not_to include('<td>Action(s)</td>')
      expect(rendered).not_to include('Change Password</button>')
      users.each do |user|
        expect(rendered).to include("<td>#{user.first_name}</td>")
        expect(rendered).to include("<td>#{user.last_name}</td>")
        expect(rendered).to include("<td>#{user.username}</td>")
        expect(rendered).to include("<td>#{user.roles.map{|u| u.name.capitalize}.join(',')}</td>")
      end
    end
  end
end
