require 'spec_helper'
require_relative '../../lib/cli'

describe 'CLI' do
  describe '.run' do
    subject { UserDirectory::CLI.run }
    let(:user) { OpenStruct.new }

    it 'gets the user and runs the Directory' do
      expect(UserDirectory::CLI::Menu::Login).to receive_message_chain(:new, :get_user) { user }
      directory_menu = double('directory_menu')
      expect(UserDirectory::CLI::Menu::Directory).to receive(:new).with(user) { directory_menu }
      expect(directory_menu).to receive(:run)
      subject
    end
  end
end