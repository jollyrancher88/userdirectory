require 'rails_helper'
require_relative '../../../lib/cli/menu/login'

module UserDirectory
  module CLI
    module Menu
      describe 'Login' do
        let(:terminal) { OpenStruct.new }

        describe '.get_user' do
          let(:menu) { Login.new(terminal) }
          subject { menu.get_user }

          context 'User selects "q"' do
            it 'exits' do
              expect(menu).to receive(:prompt_user_for_action) { 'q' }
              expect(menu).to receive(:exit_cli) { return }

              # Simulate Exit
              begin
                subject
              rescue LocalJumpError
              end
            end
          end

          context 'User selects "l"' do
            context 'User fails to login, then quits' do
              it 'Prompts, calls login, Prompts then exits' do
                expect(menu).to receive(:prompt_user_for_action).and_return('l')
                expect(menu).to receive(:log_in_user) { false }

                expect(menu).to receive(:prompt_user_for_action).and_return('q')
                expect(menu).to receive(:exit_cli) { return }

                # Simulate Exit
                begin
                  subject
                rescue LocalJumpError
                end
              end
            end

            context 'User logs in successfully' do
              let(:user) { OpenStruct.new }
              before :each do
                expect(menu).to receive(:prompt_user_for_action).and_return('l')
                expect(menu).to receive(:log_in_user) { user }
              end

              it {is_expected.to eq user}
            end
          end
        end

        describe '.log_in_user' do
          subject { Login.new(terminal).send(:log_in_user) }

          shared_examples 'login failed' do
            it 'outputs Login Failed!' do
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:say).with("Login Failed!")
              expect(terminal).to receive(:say).with("\n")
              subject
            end
          end

          context 'Invalid Login' do
            context 'Invalid Username' do
              let(:username) { '' }

              before :each do
                expect(User).to receive(:find_by).with(username: username) { nil }
                expect(terminal).to receive(:ask).with('Username: ') { username }
                allow(terminal).to receive(:ask).with('Password: ')
              end

              context 'Username is blank' do
                it_behaves_like 'login failed'
              end

              context 'User does not exist' do
                let(:username) { 'bad_username' }
                it_behaves_like 'login failed'
              end
            end

            context 'Invalid Password' do
              let(:username) { 'valid_username' }
              let(:password) { '' }
              let(:user) { OpenStruct.new(authenticate: nil) }

              before :each do
                expect(User).to receive(:find_by).with(username: username) { user }
                expect(user).to receive(:authenticate).with(password) { false }
                expect(terminal).to receive(:ask).with('Username: ') { username }
                expect(terminal).to receive(:ask).with('Password: ') { password }
              end

              it_behaves_like 'login failed'
            end
          end

          context 'Valid Login' do
            let(:username) { 'valid_username' }
            let(:password) { 'valid_password' }
            let(:user) { OpenStruct.new(authenticate: nil) }

            before :each do
              expect(User).to receive(:find_by).with(username: username) { user }
              expect(user).to receive(:authenticate).with(password) { true }
              expect(terminal).to receive(:ask).with('Username: ') { username }
              expect(terminal).to receive(:ask).with('Password: ') { password }
            end

            it { is_expected.to eq user }
          end
        end
      end
    end
  end
end