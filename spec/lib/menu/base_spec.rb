require 'spec_helper'
require_relative '../../../lib/cli/menu/base'

module UserDirectory
  module CLI
    module Menu
      describe 'Base' do
        describe '._valideate_actions' do
          subject { Base.new.send(:_validate_actions, actions) }

          context 'actions are invalid' do
            context 'actions == {}'  do
              let(:actions) { {} }

              it 'raises ArgumentError' do
                expect {subject}.to raise_error do |error|
                  expect(error).to be_a ArgumentError
                  expect(error.message).to eq 'actions must not be empty'
                end
              end
            end

            context 'actions == nil'  do
              let(:actions) { [] }

              it 'raises ArgumentError' do
                expect {subject}.to raise_error do |error|
                  expect(error).to be_a ArgumentError
                  expect(error.message).to eq 'actions must be a hash'
                end
              end
            end

            context 'Duplicate Action Codes' do
              let(:actions) {
                {'List' => 'q',
                'Quit' => 'q'}
              }

              it 'raises ArgumentError' do
                expect {subject}.to raise_error do |error|
                  expect(error).to be_a ArgumentError
                  expect(error.message).to eq 'Action Codes must be unique!'
                end
              end
            end
          end

          context 'actions are valid' do
            let(:actions) { Base::ACTIONS }
            it 'returns actions' do
              expect(subject).to eq actions
            end
          end
        end

        describe '._validate_prompt_message' do
          subject { Base.new.send(:_validate_prompt_message, message) }

          context 'message is a String' do
            let(:message) { 'Consider yourself prompted.' }
            it 'returns the message' do
              expect(subject).to eq message
            end
          end

          context 'message is not a String' do
            let(:message) { :not_a_string }
            it 'returns the message' do
              expect{subject}.to raise_error do |error|
                expect(error).to be_a ArgumentError
                expect(error.message).to eq 'prompt_message must be a String'
              end
            end
          end
        end

        describe '.prompt_user_for_action' do
          let(:actions) { {'Quit' => 'q'} }
          let(:terminal) { OpenStruct.new }
          let(:prompt_message) { '' }
          subject { Base.new(prompt_message, actions, terminal).send(:prompt_user_for_action) }

          context 'initial prompt' do
            it 'shows the menu without displaying "Invalid Action"' do
              allow(actions).to receive_message_chain(:values, :uniq) { ['q'] }
              allow(actions).to receive_message_chain(:values) { ['q'] }

              # Outputs the menu
              expect(terminal).to receive(:say).with(prompt_message)
              expect(terminal).to receive(:say).with('Actions:')
              expect(terminal).to receive(:say).with("\tQuit - q")

              # User selects 'q'
              action = 'q'
              expect(terminal).to receive(:ask).with('->') { action }

              expect(subject).to eq 'q'
            end
          end

          context 'successive prompt' do
            it 'Shows Invalid Action in the output' do
              allow(actions).to receive_message_chain(:values, :uniq) { ['q'] }
              allow(actions).to receive_message_chain(:values) { ['q'] }

              # Outputs the menu
              expect(terminal).to receive(:say).with(prompt_message)
              expect(terminal).to receive(:say).with('Actions:')
              expect(terminal).to receive(:say).with("\tQuit - q")

              # User selects 'blah'
              action = 'blah'
              expect(terminal).to receive(:ask).with('->').and_return(action)

              # Outputs "Invalid Action"
              expect(terminal).to receive(:say).with("\nInvalid Action\n")
              expect(terminal).to receive(:say).with("\n")

              # Outputs the menu
              expect(terminal).to receive(:say).with(prompt_message)
              expect(terminal).to receive(:say).with('Actions:')
              expect(terminal).to receive(:say).with("\tQuit - q")

              # User selects 'q'
              action = 'q'
              expect(terminal).to receive(:ask).with('->') { action }

              expect(subject).to eq 'q'
            end
          end
        end

        describe '.quit_selected?' do
          let(:action) { 'q' }
          subject { Base.new.send(:quit_selected?, action) }

          it { is_expected.to eq true }

          context 'action is not "q"' do
            let(:action) { 'l' }
            it { is_expected.to eq false }
          end
        end

        describe '.exit_cli' do
          let(:actions) { {'Quit' => 'q'} }
          let(:terminal) { OpenStruct.new }
          subject { Base.new('', actions, terminal).send(:exit_cli) }

          it 'Exits the program' do
            expect(terminal).to receive(:say).with('Goodbye.')
            expect { subject }.to raise_error SystemExit
          end
        end
      end
    end
  end
end
