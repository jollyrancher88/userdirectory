require 'rails_helper'
require_relative '../../../lib/cli/menu/directory'

module UserDirectory
  module CLI
    module Menu
      describe 'Directory' do
        let(:user) { OpenStruct.new }
        let(:terminal) { OpenStruct.new }

        before :each do
          allow(user).to receive(:is_a?).with(User).and_return(true)
        end

        describe '.run' do
          let(:directory) { Directory.new(user, terminal) }
          subject { directory.run }

          context 'user selects "q"' do
            it 'exits' do
              expect(directory).to receive(:prompt_user_for_action) { 'q' }
              expect(directory).to receive(:exit_cli) { return }

              # Simulate Exit
              begin
                subject
              rescue LocalJumpError
              end
            end
          end

          context 'user selects "l" and then "q"' do
            it 'calls .list_users first, then exits' do

              # Executes .list_user
              expect(directory).to receive(:prompt_user_for_action).and_return('l')
              expect(directory).to receive(:list_users)

              # Executes calls .run again

              # "exits"
              expect(directory).to receive(:prompt_user_for_action) { 'q' }
              expect(directory).to receive(:exit_cli) { return }

              # Simulate Exit
              begin
                subject
              rescue LocalJumpError
              end
            end
          end

          context 'user selects "c" and then "q"' do
            it 'calls .change_password first, then exits' do

              # Executes .list_user
              expect(directory).to receive(:prompt_user_for_action).and_return('c')
              expect(directory).to receive(:change_password)

              # Executes calls .run again

              # "exits"
              expect(directory).to receive(:prompt_user_for_action) { 'q' }
              expect(directory).to receive(:exit_cli) { return }

              # Simulate Exit
              begin
                subject
              rescue LocalJumpError
              end
            end
          end
        end

        describe '.actions' do
          subject { UserDirectory::CLI::Menu::Directory.new(user, terminal).send(:actions) }

          context 'current_user can write' do
            let(:user) { OpenStruct.new(can_write?: true)}
            it { is_expected.to eq Directory::ACTIONS }
          end

          context 'current_user cannot write' do
            it { is_expected.to eq Directory::READ_ONLY_ACTIONS }
          end
        end

        describe '.list_users' do
          subject { UserDirectory::CLI::Menu::Directory.new(user, terminal).send(:list_users) }
          let(:users) { [] }
          let(:table) { 'fake_table' }

          context 'No Users exist' do
            # Who knows how you go this far but... :)
            it 'prints the table anyway' do
              expect(Terminal::Table).to receive(:new) { table }
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:say).with(table)
              expect(terminal).to receive(:say).with("\n")
              subject
            end
          end

          context 'Users exist' do
            let(:users) { [OpenStruct.new(first_name: 'first', last_name: 'last', username:'first.last', roles: [OpenStruct.new(name: 'guest')])] }

            it 'prints the table' do
              expect(Terminal::Table).to receive(:new) { table }
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:say).with(table)
              expect(terminal).to receive(:say).with("\n")
              subject
            end
          end
        end

        describe '.change_password' do
          subject { UserDirectory::CLI::Menu::Directory.new(user, terminal).send(:change_password) }

          let(:username) { 'fake_username' }
          let(:password) { 'fake_password' }

          context 'Username is blank' do
            let(:username) { '' }
            it 'Displays and error and returns' do
              expect(terminal).to receive(:ask).with('Username: ') { username }
              expect(User).to receive(:find_by).with(username: username) { nil }
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:say).with("Username cannot be blank")
              expect(terminal).to receive(:say).with("\n")
              subject
            end
          end

          context 'User does not exist' do
            it 'Displays and error and returns' do
              expect(terminal).to receive(:ask).with('Username: ') { username }
              expect(User).to receive(:find_by).with(username: username) { nil }
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:say).with("User with username '#{username}' does not exist.")
              expect(terminal).to receive(:say).with("\n")
              subject
            end
          end

          context 'Invalid password' do
            context 'password blank' do
              let(:user) { OpenStruct.new(errors: password_errors, update: nil) }
              let(:password) { '' }
              let(:password_errors) { ['Password can\'t be blank', 'Password is too short (minimum is 6 characters)'] }
              it 'Displays and error and returns' do
                allow(user).to receive(:update).with(password: password, password_confirmation: password).and_return(false)

                expect(terminal).to receive(:ask).with('Username: ') { username }
                expect(User).to receive(:find_by).with(username: username) { user }
                expect(terminal).to receive(:say).with("\n")
                expect(terminal).to receive(:ask).with('New Password: ') { password }
                expect(terminal).to receive(:say).with("\n")
                expect(terminal).to receive(:say).with(password_errors.join(', '))
                expect(terminal).to receive(:say).with("\n")
                subject
              end
            end

            context 'password is too short' do
              let(:user) { OpenStruct.new(errors: password_errors, update: nil) }
              let(:password) { '123' }
              let(:password_errors) { ['Password is too short (minimum is 6 characters)'] }
              it 'Displays and error and returns' do
                allow(user).to receive(:update).with(password: password, password_confirmation: password).and_return(false)

                expect(terminal).to receive(:ask).with('Username: ') { username }
                expect(User).to receive(:find_by).with(username: username) { user }
                expect(terminal).to receive(:say).with("\n")
                expect(terminal).to receive(:ask).with('New Password: ') { password }
                expect(terminal).to receive(:say).with("\n")
                expect(terminal).to receive(:say).with(password_errors.join(', '))
                expect(terminal).to receive(:say).with("\n")
                subject
              end
            end
          end

          context 'Valid Password Change' do
            let(:user) { OpenStruct.new(update: nil) }
            it 'Displays Success message and returns' do
              allow(user).to receive(:update).with(password: password, password_confirmation: password).and_return(true)

              expect(terminal).to receive(:ask).with('Username: ') { username }
              expect(User).to receive(:find_by).with(username: username) { user }
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:ask).with('New Password: ') { password }
              expect(terminal).to receive(:say).with("\n")
              expect(terminal).to receive(:say).with("Success! Password Changed for User(#{user.username}).")
              expect(terminal).to receive(:say).with("\n")
              subject
            end
          end
        end
      end
    end
  end
end
