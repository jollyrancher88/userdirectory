require 'rails_helper'

RSpec.describe User, :type => :model do

  shared_examples 'save_successful' do
    it 'saves successfully' do
      expect(subject.save).to eq true
    end
  end

  subject { User.new(username: 'warmer.buffet',
                     first_name: 'Warmer',
                     last_name: 'Buffet',
                     password: password,
                     password_confirmation: confirmation,
                     roles: roles)
  }

  let(:password) { 'ILoveToEatAtValentinos' }
  let(:confirmation) { 'ILoveToEatAtValentinos' }
  let(:roles) { [] }

  describe '.save' do
    context 'password' do
      context 'password & confirmation match' do
        it_behaves_like 'save_successful'
      end

      context 'password/confirmation mismatch' do
        let(:confirmation) { 'I Do Not Like Valentinos' }

        it 'fails to save' do
          expect(subject.save).to eq false
          expect(subject.errors.to_a).to include ("Password confirmation doesn't match Password")
        end
      end
    end
  end

  describe '.can_read?' do
    subject { User.new(roles: roles).can_read? }

    context 'no roles set' do
      let(:roles) { [] }
      it { is_expected.to eq false }
    end

    context 'roles have :read permission' do
      (Roles.list.permutation(1).to_a + Roles.list.permutation(2).to_a + Roles.list.permutation(3).to_a).each do |role_array|
        context "roles include: #{role_array.map{|r| r.name}.join(',')}" do
          let(:roles) { role_array }
          it { is_expected.to eq true }
        end
      end
    end
  end

  describe '.can_write?' do
    subject { User.new(roles: roles).can_write? }

    context 'no roles set' do
      let(:roles) { [] }
      it { is_expected.to eq false }
    end

    context 'roles have :write permission' do
      role_list = [Roles::ADMIN, Roles::EMPLOYEE]
      (role_list.permutation(1).to_a + role_list.permutation(2).to_a + role_list.permutation(3).to_a).each do |role_array|
        context "roles include: #{role_array.map{|r| r.name}.join(',')}" do
          let(:roles) { role_array }
          it { is_expected.to eq true }
        end
      end
    end

    context 'roles do not have :write permission' do
      let(:roles) { [Roles::GUEST] }
      it { is_expected.to eq false }
    end
  end


  describe '.can_delete?' do
    subject { User.new(roles: roles).can_delete? }

    context 'no roles set' do
      let(:roles) { [] }
      it { is_expected.to eq false }
    end

    context 'roles have :delete permission' do
      role_list = [Roles::ADMIN]
      (role_list.permutation(1).to_a + role_list.permutation(2).to_a + role_list.permutation(3).to_a).each do |role_array|
        context "roles include: #{role_array.map{|r| r.name}.join(',')}" do
          let(:roles) { role_array }
          it { is_expected.to eq true }
        end
      end
    end

    context 'roles do not have :delete permission' do
      let(:roles) { [Roles::EMPLOYEE, Roles::GUEST] }
      it { is_expected.to eq false }
    end
  end
end
