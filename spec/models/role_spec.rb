require 'rails_helper'

RSpec.describe Role, type: :model do

  subject { Role.new(name: name,
                     permissions: permissions)
  }

  describe '.permissions_valid' do
    let(:name) {'role_name'}
    context 'permissions nil' do
      let(:permissions) { nil }

      it 'fails to save' do
        expect(subject.save).to eq false
        expect(subject.errors.to_a).to include ("Permissions can't be blank")
      end
    end

    context 'permissions empty' do
      let(:permissions) { [] }

      it 'fails to save' do
        expect(subject.save).to eq false
        expect(subject.errors.to_a).to include ("Permissions can't be blank")
      end
    end

    context 'permissions invalid' do
      invalid_permissions = [nil,1,'two']
      invalid_permissions.each do |invalid|
        context "permissions = [#{invalid}]" do
          let(:permissions) { [invalid] }
          it 'fails to save' do
            expect(subject.save).to eq false
            expect(subject.errors.to_a).to include "Permissions - Invalid permission (#{invalid})"
          end
        end
      end
    end

    context 'permissions are valid' do
      Role::PERMISSIONS.each do |permission|
        context "permissions = [#{permission}]" do
          let(:permissions) { [permission] }
          it 'return true' do
            expect(subject.send(:permissions_valid)).to eq true
          end
        end
      end


    end
  end
end
