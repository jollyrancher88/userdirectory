require 'rails_helper'

RSpec.describe SessionsHelper, type: :helper do
  describe '.full_title' do
    let(:page_title) { '' }
    let(:base_title) { 'User Directory' }
    subject { helper.full_title(page_title)}

    context 'page_title is empty' do
      it {is_expected.to eq base_title}
    end

    context 'page_title is not empty' do
      let(:page_title) { 'User List' }
      it {is_expected.to eq "#{page_title} | #{base_title}"}
    end
  end
end
