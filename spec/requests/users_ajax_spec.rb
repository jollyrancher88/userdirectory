require 'rails_helper'

RSpec.describe "Users ajax", type: :request do
  include SessionsHelper


  describe '/users/change_password' do
    subject {  post '/users/change_password', params: {username: username, password: password} }
    let(:username) { 'billybob' }
    let(:password) { 'superbob123' }

    context 'No User logged in' do
      it 'returns 401' do
        subject
        expect(response).to have_http_status(401)
        expect(body).to eq "{\"message\":[\"Unauthorized\"]}"
      end
    end

    context 'User is not allowed to write' do
      before do
        guest_user = User.find_by(username: 'billybob') ||
            User.create(username: 'billybob',
                        first_name: 'Billy',
                        last_name: 'Bob',
                        password: 'super123',
                        password_confirmation: 'super123',
                        roles: [Roles::GUEST])

        post '/login', params: {session: {username: guest_user.username, password: 'super123'}}
      end

      after do
        get '/logout'
      end

      it 'returns 403' do
        subject
        expect(response).to have_http_status(403)
        expect(body).to eq "{\"message\":[\"Forbidden\"]}"
      end
    end

    context 'User is allowed to write' do
      before :each do
        employee_user = User.find_by(username: 'builderbob') ||
            User.create(username: 'builderbob',
                        first_name: 'Billy',
                        last_name: 'Bob',
                        password: 'super123',
                        password_confirmation: 'super123',
                        roles: [Roles::EMPLOYEE])

        post '/login', params: {session: {username: employee_user.username, password: 'super123'}}
      end

      after :all do
        get '/logout'
      end

      context 'Username is blank' do
        let(:username) { '' }

        it 'returns 200, with errors' do
          subject
          expect(response).to have_http_status(200)
          response = JSON.parse(body)
          expect(response['success']).to eq false
          expect(response['message']).to eq 'No username provided!'
        end
      end

      context 'Password is blank' do
        let(:password) { '' }

        it 'returns 200, with errors' do
          subject
          expect(response).to have_http_status(200)
          response = JSON.parse(body)
          expect(response['success']).to eq false
          expect(response['message']).to eq 'No password provided!'
        end
      end

      context 'User DNE for provided username' do
        let(:username) { 'DNE' }

        it 'returns 200, with errors' do
          subject
          expect(response).to have_http_status(200)
          response = JSON.parse(body)
          expect(response['success']).to eq false
          expect(response['message']).to eq "User (#{username}) not found!"
        end
      end

      context 'Password is too short' do
        let(:username) { 'builderbob'}
        let(:password) { '123' }

        it 'returns 200, with errors' do
          subject
          expect(response).to have_http_status(200)
          response = JSON.parse(body)
          expect(response['success']).to eq false
          expect(response['message']).to eq 'Password is too short (minimum is 6 characters)'
        end
      end

      context 'Username and Password are valid' do
        let(:username) { 'builderbob'}
        let(:password) { 'builder123' }

        it 'returns 200, with success' do
          subject
          expect(response).to have_http_status(200)
          response = JSON.parse(body)
          expect(response['success']).to eq true
          expect(response['message']).to eq 'success'
        end
      end
    end
  end

  # describe 'GET #index' do
  #   context 'A user is logged in' do
  #     before do
  #       guest_user = User.find_by(username: 'billybob') ||
  #                    User.create(username: 'billybob',
  #                                first_name: 'Billy',
  #                                last_name: 'Bob',
  #                                password: 'super123',
  #                                password_confirmation: 'super123',
  #                                roles: [Roles::GUEST])
  #       log_in(guest_user)
  #     end
  #
  #     after do
  #       log_out
  #     end
  #
  #     it 'returns http success' do
  #       get :index
  #       expect(response).to have_http_status(:success)
  #     end
  #   end
  #
  #   context 'No user logged in' do
  #     it 'returns 302 and redirects' do
  #       get :index
  #       expect(response).to have_http_status(302)
  #       expect(response.body).to include 'You are being '
  #       expect(response.body).to include 'redirected'
  #     end
  #   end
  # end
end
