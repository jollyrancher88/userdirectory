require_relative 'cli/menu/login'
require_relative 'cli/menu/directory'

module UserDirectory
  module CLI
    class << self
      def run
        current_user = Menu::Login.new.get_user
        Menu::Directory.new(current_user).run
      end
    end
  end
end

