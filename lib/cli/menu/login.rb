require_relative 'base'
module UserDirectory
  module CLI
    module Menu
      class Login < Base
        PROMPT_MESSAGE = 'Please Login!'

        ACTIONS = {
                    'Login' => 'l',
                    'Quit' => 'q'
                  }

        def initialize(terminal = HighLine)
          super(PROMPT_MESSAGE, ACTIONS, terminal)
        end

        def get_user
          action = prompt_user_for_action
          exit_cli if quit_selected?(action)

          user = log_in_user # The only other valid option is 'Login'

          user || get_user # return user if successful otherwise rerun the menu
        end

        private
        def log_in_user
          username = @terminal.ask('Username: ').downcase
          password = @terminal.ask('Password: ') { |q| q.echo = '*' }
          user = User.find_by(username: username)

          if user.nil? || user.authenticate(password) == false
            @terminal.say "\n"
            @terminal.say "Login Failed!"
            @terminal.say "\n"
            false
          else
            user
          end
        end
      end
    end
  end
end
