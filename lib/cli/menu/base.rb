require 'highline/import'
module UserDirectory
  module CLI
    module Menu
      class Base
        PROMPT_MESSAGE = ''
        ACTIONS = {'Quit' => 'q'}

        attr_reader :prompt_message
        attr_reader :actions

        def initialize(prompt_message = PROMPT_MESSAGE, actions = ACTIONS, terminal = HighLine)
          @prompt_message = _validate_prompt_message(prompt_message)
          @actions = _validate_actions(actions)
          @terminal = terminal
        end

        private
        def display_menu
          @terminal.say prompt_message
          @terminal.say 'Actions:'
          display_actions
        end

        def display_actions
          actions.each do |label, code|
            @terminal.say "\t#{label} - #{code.downcase}"
          end
        end

        def quit_selected?(action)
          action.downcase == 'q'
        end

        def exit_cli
          @terminal.say('Goodbye.')
          exit
        end

        def prompt_user_for_action
          action = nil
          until actions.values.include?(action)
            unless action.nil?
              @terminal.say "\nInvalid Action\n"
              @terminal.say "\n"
            end
            display_menu
            action = @terminal.ask('->').downcase
          end
          action
        end

        def _validate_actions(actions)
          raise ArgumentError, 'actions must be a hash' unless actions.is_a?(Hash)
          raise ArgumentError, 'actions must not be empty' if actions.empty?
          raise ArgumentError, 'Action Codes must be unique!' unless actions.values == actions.values.uniq
          actions
        end

        def _validate_prompt_message(prompt_message)
          raise ArgumentError, 'prompt_message must be a String' unless prompt_message.is_a?(String)
          prompt_message
        end
      end
    end
  end
end