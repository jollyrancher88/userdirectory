require_relative 'base'
module UserDirectory
  module CLI
    module Menu
      class Directory < Base
        PROMPT_MESSAGE = 'Welcome to the User Directory!'

        READ_ONLY_ACTIONS = {
                              'List Users' => 'l',
                              'Quit' => 'q'
                            }

        ACTIONS = { "Change a User's Password" => 'c' }.merge!(READ_ONLY_ACTIONS)


        def initialize(user, terminal = HighLine)
          raise ArgumentError 'user must be of type User' unless user.is_a?(User)
          @current_user = user
          super(PROMPT_MESSAGE, ACTIONS, terminal)
        end

        def run
          action = prompt_user_for_action

          exit_cli if quit_selected?(action)
          list_users if action == ACTIONS['List Users']
          change_password if action == ACTIONS["Change a User's Password"]

          run # continue to run the Directory until the user selects 'Quit'
        end

        def actions
          @current_user.can_write? ? ACTIONS : READ_ONLY_ACTIONS
        end

        private
        def list_users
          rows = User.all.map do |user|
            [user.first_name, user.last_name, user.username, user.roles.map(&:name).join(',')]
          end

          table = Terminal::Table.new(title: 'User List:', headings: ['First Name', 'Last Name', 'Username', 'Roles'], rows: rows)

          @terminal.say "\n"
          @terminal.say table
          @terminal.say "\n"
        end

        def change_password
          username = @terminal.ask('Username: ').downcase
          user = User.find_by(username: username)

          @terminal.say "\n"
          if user
            password = @terminal.ask('New Password: ') { |q| q.echo = '*' }

            @terminal.say "\n"
            if user.update(password: password, password_confirmation: password)
              @terminal.say "Success! Password Changed for User(#{user.username})."
            else
              @terminal.say user.errors.to_a.join(', ')
            end
          else
            @terminal.say username.empty? ? 'Username cannot be blank' : "User with username '#{username}' does not exist."
          end
          @terminal.say "\n"
        end
      end
    end
  end
end
