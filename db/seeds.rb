admin_role = Role.find_by(name: 'admin') || Role.create(name: 'admin', permissions: Role::PERMISSIONS)
employee_role = Role.find_by(name: 'employee') || Role.create(name: 'employee', permissions: [:read, :write])
guest_role = Role.find_by(name: 'guest') || Role.create(name: 'guest', permissions: [:read])

User.create(username: 'superman',
            first_name: 'Clark',
            last_name: 'Kent',
            password: 'supervision',
            password_confirmation: 'supervision',
            roles: [admin_role]) unless User.find_by(username: 'superman')

User.create(username: 'batman',
            first_name: 'Bruce',
            last_name: 'Wayne',
            password: 'batmobile',
            password_confirmation: 'batmobile',
            roles: [employee_role]) unless User.find_by(username: 'batman')

User.create(username: 'flash',
            first_name: 'Barry',
            last_name: 'Allen',
            password: 'toofasttosee',
            password_confirmation: 'toofasttosee',
            roles: [guest_role]) unless User.find_by(username: 'flash')