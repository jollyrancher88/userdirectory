Rails.application.routes.draw do
  get  '',                         to: 'users#index'
  get  '/users',                   to: 'users#index'
  post '/users/change_password',   to: 'users#change_password'

  get  '/login',                   to: 'sessions#new'
  post '/login',                   to: 'sessions#create'
  get  '/logout',                  to: 'sessions#destroy'
end
