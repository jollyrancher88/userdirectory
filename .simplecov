# Start coverage
SimpleCov.start do
  add_filter '/spec/'
  add_filter 'config'
  add_filter 'app/channels'
  add_group 'Controllers', 'app/controllers'
  add_group 'Helpers', 'app/helpers'
  add_group 'Models', 'app/models'
  add_group 'Lib', 'lib/'
end