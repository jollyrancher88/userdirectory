module UsersHelper
  def can_read?
    self.roles.any?{|r| r.permissions.include?(:read) }
  end

  def can_write?
    self.roles.any?{|r| r.permissions.include?(:write) }
  end

  def can_delete?
    self.roles.any?{|r| r.permissions.include?(:delete) }
  end
end
