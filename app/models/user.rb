class User < ApplicationRecord
  include UsersHelper

  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 3..20 }

  has_many :user_role_assignments
  has_many :roles, through: :user_role_assignments
end
