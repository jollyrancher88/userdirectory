class Role < ApplicationRecord
  PERMISSIONS = [:read, :write, :delete]

  validates :name, presence: true, uniqueness: true
  validates :permissions, presence: true, uniqueness: true
  serialize :permissions, Array
  validate :permissions_valid
  attr_readonly :name, :permissions

  has_many :user_role_assignments
  has_many :users, through: :user_role_assignments

  private
  def permissions_valid
    return if self.permissions.empty?
    self.permissions.reject {|p| PERMISSIONS.include?(p) }.each do |value|
      self.errors.add :permissions, "- Invalid permission (#{value})"
    end
    true
  end
end

module Roles
  ADMIN = Role.find_by(name: 'admin') if Role.find_by(name: 'admin')
  EMPLOYEE = Role.find_by(name: 'employee') if Role.find_by(name: 'employee')
  GUEST = Role.find_by(name: 'guest') if Role.find_by(name: 'guest')

  def self.list
    [ADMIN, EMPLOYEE, GUEST]
  end
end