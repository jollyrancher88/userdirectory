@showOverlay = (first_name, last_name, username) ->
  $('#full_name').html(first_name + ' ' + last_name)
  $('#username').html(username)
  $('#overlay').show()

@hideOverlay = () -> $('#overlay').hide()

$ ->
  $("#change_password").on "click", ->
    $('#message').removeClass('error')
    $('#message').removeClass('success')
    $('#message').hide()
    username = $('#username').html()
    password = $('#password').val()
    $.ajax {
      url: '/users/change_password'
      type: 'post'
      dataType: 'json'
      data: {
        username: username
        password: password
      }
      success: (response) ->
        if response.success == true
          $('#message').addClass('success')
          $('#message').html("Success! Password Changed!")
          $('#message').show()
        else
          $('#message').html(response.message)
          $('#message').addClass('error')
          $('#message').show()
    }