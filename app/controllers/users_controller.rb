class UsersController < ApplicationController
  def index
    redirect_to '/login' and return unless logged_in?
    @users = User.all
    @can_edit = current_user && current_user.can_write?
  end

  def change_password
    render :json => { message: ['Unauthorized'] }, :status => 401 and return if current_user.nil?
    render :json => { message: ['Forbidden'] }, :status => 403 and return unless current_user.can_write?

    success = true
    message = 'success'

    username = params[:username].to_s
    password = params[:password].to_s

    render json: {success: false, message: 'No username provided!'} and return if username.empty?
    render json: {success: false, message: 'No password provided!'} and return if password.empty?
    user = User.find_by(username: username)
    render json: {success: false, message: "User (#{username}) not found!"} and return unless user

    unless user.update(password: password, password_confirmation: password)
      success = false
      message = user.errors.to_a.join(', ')
    end

    render :json => { success: success, message: message }
  end
end
