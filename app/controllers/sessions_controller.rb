class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(username: params[:session][:username].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      flash[:success] = "Welcome to #{user.first_name}!"
      # redirect_to @user
      log_in(user)
      redirect_to '/users'
    else
      # Create an error message.
      flash[:error] = 'Login Failed!'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to '/login'
  end
end
