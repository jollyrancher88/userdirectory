#!/bin/bash          

# Install the gems
bundle install

# Prep the local db
rake db:create
rake db:migrate
rake db:seed

# Prep the test db
rake db:migrate RAILS_ENV=test
rake db:seed RAILS_ENV=test --trace
