# README

The UserDirectory is a simple to use application for maintaining your users and their security roles.

![Build Status](https://app.codeship.com/projects/c08e1240-42f0-0135-2087-7ac6334171ad/status?branch=master)

# Requirements: #

* Ruby 2.4
* [bundler](https://rubygems.org/gems/bundler)

# Setup #
```
$ git clone https://jollyrancher88@bitbucket.org/jollyrancher88/userdirectory.git
$ cd userdirectory
$ ./script/setup.sh
```

# Seed Users #
The setup script will create 3 users in the Database. You can use a username and password from the table below to log into the application:

Username  | Password | Can Change Passwords
------------- | ------------- | 
`superman`  | `supervision` | yes
`batman`  | `batmobile` | yes
`flash`  | `toofasttosee` | no


# Execution #
To start the Rails app:  `rails s`

To start the Command Line Interface: `rake cli`
# Tests #
To run the test suite with a generated coverage report: `rspec`

# Wiki #
For more information, check out these wiki pages:

* [About the Rails App](https://bitbucket.org/jollyrancher88/userdirectory/wiki/Home)
* [Command Line Interface](https://bitbucket.org/jollyrancher88/userdirectory/wiki/Command%20Line%20Interface)
* [Testing](https://bitbucket.org/jollyrancher88/userdirectory/wiki/Testing)